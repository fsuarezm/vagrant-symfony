# Vagrant Symfony 2 Box setup 

A Vagrant build to run a Symfony2 development LAMP stack (Apache, Mysql, Phpmyadmin, PHP, Node.js, Less, Composer, Imagic, ...) on a vmware or virtualbox provider.

## Install

#### This setup is based and tested with Ubuntu Precise 64 bit base box.

* Clone this repository

    ```$ git clone git@bitbucket.org:fsuarezm/vagrant-symfony.git```

* Install git submodules

    ```$ git submodule update --init```

* run vagrant (for the first time it should take up to 10-15 min)
    ```$ vagrant up```
    
* Web server is accessible with http://10.10.10.10 (IP address can be changed in Vagrantfile)

* PhpMyAdmin is accessible with http://10.10.10.10:8000

* Vagrant automatically setups database with this setup:

    * Username: symfony
    * Password: symfony1
    * Database: symfony

* Change ```app.php```

        $env = getenv('APP_ENV') ? getenv('APP_ENV') : 'prod';
        $debug = (bool)getenv('APP_DEBUG');
        $kernel = new AppKernel($env, $debug);

## Installed components

* Apache
* MySQL
* PhpMyAdmin
* Git
* Composer
* Vim
* Curl
* Node.js
* less
* Imagic
* memcached

* Optional PHP 5.5 commented at ```puppet/manifest/default.pp```

## Thanks to

* [example42](https://github.com/example42) - for great nginx\mysql templates
* [caramba1337](https://github.com/caramba1337) - for great ideas

## Hints
####Startup speed
To speed up the startup process use ```$ vagrant up --no-provision``` (thanks to [caramba1337](https://github.com/caramba1337))