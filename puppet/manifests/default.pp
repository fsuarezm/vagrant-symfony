$doc_root     = '/var/www/web'
$php_modules  = [ 'imagick', 'curl', 'mysql', 'cli', 'intl', 'mcrypt', 'memcache' ]
$sys_packages = [ 'build-essential', 'curl', 'vim' ]
$mysql_host   = 'localhost'
$mysql_db     = 'symfony'
$mysql_user   = 'symfony'
$mysql_pass   = 'symfony1'
$pma_port     = 8000

Exec { path => [ "/bin/", "/sbin/", "/usr/bin/", "/usr/sbin/" ] }

exec { 'apt-get update':
    command => 'apt-get update',
}

class { 'apt':
    always_apt_update => true,
}

class { 'git': }

package { ['python-software-properties']:
    ensure  => 'installed',
    require => Exec['apt-get update'],
}

package { $sys_packages:
    ensure  => 'installed',
    require => Exec['apt-get update'],
}

class { 'apache': }

apache::module { 'rewrite': }
apache::vhost { 'default':
    docroot                  => $doc_root,
    directory                => $doc_root,
    directory_allow_override => 'All',
    server_name              => false,
    env_variables            => [ "APP_ENV dev", "APP_DEBUG true" ],
    priority                 => '',
    template                 => 'vagrant/apache/vhost.conf.erb',
}

/* PHP 5.5
apt::ppa { 'ppa:ondrej/php5':
    before => Class['php'],
}
/*
/* PHP 5.4 */
apt::ppa { 'ppa:ondrej/php5-oldstable':
    before => Class['php'],
}

class { 'php': }

php::module { $php_modules: }

vagrant::phpini { 'php':
    value => ['date.timezone = "UTC"', 'upload_max_filesize = 8M', 'short_open_tag = 0'],
}

class { 'phpunit': }

class { 'mysql':
    root_password => 'root',
    require       => Exec['apt-get update'],
}

mysql::grant { $mysql_db:
    mysql_privileges     => 'ALL',
    mysql_db             => $mysql_db,
    mysql_user           => $mysql_user,
    mysql_password       => $mysql_pass,
    mysql_host           => $mysql_host,
    mysql_grant_filepath => '/home/vagrant/puppet-mysql',
}

package { 'phpmyadmin':
    require => Class[ 'mysql' ],
}

apache::vhost { 'phpmyadmin':
    server_name => false,
    docroot     => '/usr/share/phpmyadmin',
    port        => $pma_port,
    priority    => '10',
    require     => Package['phpmyadmin'],
    template    => 'vagrant/apache/vhost.conf.erb',
}

class composer {
    exec { 'install composer php dependency management':
        command => 'curl -s http://getcomposer.org/installer | php -- --install-dir=/usr/bin && mv /usr/bin/composer.phar /usr/bin/composer',
        creates => '/usr/bin/composer',
        require => [Package['php5-cli'], Package['curl']],
    }

    exec { 'composer self update':
        command => 'composer self-update',
        require => [Package['php5-cli'], Package['curl'], Exec['install composer php dependency management']],
    }
}

class nodejs {
    exec { 'curl -sL https://deb.nodesource.com/setup | bash -':
        command => 'curl -sL https://deb.nodesource.com/setup | bash -'
    }

    package{ 'nodejs':
        require => Exec['curl -sL https://deb.nodesource.com/setup | bash -'],
    }

    exec { 'install less':
        command => 'npm install -g less',
        require => Package['nodejs'],
    }
}

include composer
include nodejs
